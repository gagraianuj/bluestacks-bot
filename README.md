# Backend Developer Challenge #

Submission for assignment to create a Discord bot and implement some basic commands.
Link for discord app server for testing and evaluation https://discord.gg/fUwTTpA

### Stuff this bot does ###

1. Replies **'hey'** in response to **'hi'**
2. Use **'!google keyword'** command to get top 5 results on google.com for the term keyword
3. Use **'!recent keyword'** command to go through your search hisory.

### Implementation Details ###
The code uses python at its core. For storing user chat history and serach related data MongoDB is used. This is hosted on Digital Ocean Cloud for testing and evaluation.

### Reference Meterial Used ###
* [How to make a dicsord bot python](https://realpython.com/how-to-make-a-discord-bot-python/)
* [MongoDB Docs](https://docs.mongodb.com/)
* [Digital Ocean Docs](https://www.digitalocean.com/docs/)
* [Discord Developers Docs](https://discordapp.com/developers/docs/intro)
* [Google Cusotm Search API](https://developers.google.com/custom-search/v1/overview)