# Hit Google Search API and return top 5 results

import os
import pprint
from googleapiclient.discovery import build
from dotenv import load_dotenv

load_dotenv()
api_key = os.getenv("GOOGLE_API_KEY")
cse_key = os.getenv("CSE_KEY")

def google_search_api(search_keyword):

    resource = build("customsearch", 'v1', developerKey=api_key).cse()
    result = resource.list(q=search_keyword, cx=cse_key).execute()

    search_response = list()
    count = 0
    try:
        for item in result['items']:
            search_response.append([item['title'], item['link']])
            count += 1
            if count > 4:
                break
    except Exception as e:
        print(e)

    if len(search_response) > 1:
        return (search_response, True)
    else:
        return (search_response, False)