from pymongo import MongoClient

client = MongoClient()
db = client['BluestacksChat']
chatcollection = db['chatLogs']

def insert_data(author, content, timestamp):
    past_data, status = get_data(author)

    if status:
        updated_timestamp = past_data['timestamp']+ [str(timestamp)]
        updated_chat = past_data['chat']+ [str(content)]

        new_data = {"$set":
                        {
                        'timestamp': updated_timestamp,
                        'chat': updated_chat
                    }}
        my_query = { "author": str(author) }
        chatcollection.update_one(my_query, new_data)

    else:
        new_data = {
            'author': str(author),
            'timestamp': [str(timestamp)],
            'chat': [str(content)]
        }
        result = chatcollection.insert_one(new_data)

def get_data(author):
    past_data = chatcollection.find_one({'author': str(author)})

    if past_data:
        return past_data, True
    else:
        return past_data, False

# insert_data('anuj8june#7421', 'hehe', '2020-04-21 11:15:26.983000')
# print(get_data('anuj8june#7421'))