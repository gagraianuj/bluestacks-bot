import discord
from .command_tracker.check_for_commands import assign_codes_for_commands
from .google_api.search_google import google_search_api
from .history.search_history import get_data, insert_data

class MainModule(discord.Client):
    async def on_ready(self):
        print(f'{self.user} has connected to Discord!')

    async def on_message(self, message):

        if (message.author != self.user):
            status_code, reply_msg = assign_codes_for_commands(message.content)

            if status_code == 1:
                await message.channel.send(reply_msg)

            if status_code == 2:
                search_keyword = reply_msg

                insert_data(message.author, search_keyword, message.created_at)
                search_results, status = google_search_api(search_keyword)

                if status:
                    await message.channel.send("Top 5 search results for '{}' on google.com".format(message.content.replace('!google ', '')))
                    for result in search_results:
                        await message.channel.send(result[0])
                        await message.channel.send(result[1])

            if status_code == 3:
                query_keyword = reply_msg
                query_result, status = get_data(message.author)

                match_keyword = list()
                match_keyword = [keyword for keyword in query_result['chat'] if query_keyword.lower() in keyword.lower()]
                
                if len(match_keyword) >= 1:
                    await message.channel.send("Your search history results for keyword '{}'".format(reply_msg))
                    for matched_key in match_keyword:
                        await message.channel.send(matched_key)
