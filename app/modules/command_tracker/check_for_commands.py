import string

def assign_codes_for_commands(msg):
    status_code = 0
    msg = msg.strip()
    if msg.lower() == 'hi':
        status_code = 1
        reply_msg = 'hey'
        return status_code, reply_msg

    if '!google ' in msg and len(msg) >=9:
        search_keyword = msg.split('!google ')[1].strip()
        if search_keyword != ' ':
            status_code = 2
            return status_code, search_keyword

    if '!recent ' in msg and len(msg) >=9:
        query_keyword = msg.split('!recent ')[1].strip()
        if query_keyword != ' ':
            status_code = 3
            return status_code, query_keyword

    return status_code, ''
 